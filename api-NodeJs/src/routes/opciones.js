const { Router } = require('express');
const router = new Router();
const _ = require('underscore');

const opciones = require('../opciones.json');

router.get('/', (req, res) => {
    res.json(opciones);
});

router.post('/', (req, res) => {
    const id = opciones.length + 1;
    const { opcion } = req.body;
    const newopcion = { ...req.body, id };
    if (id && opcion) {
        opciones.push(newopcion);
        res.json(opciones);
    } else {
        res.status(500).json({error: 'There was an error.'});
    }
});

router.put('/:id', (req, res) => {
    const { id } = req.params;
    const { opcion } = req.body;
    if (id && opcion) {
        _.each(opciones, (respuesta, i) => {
            if (respuesta.id === id) {
                respuesta.opcion = opcion;
            }
        });
        res.json(opciones);
    } else {
        res.status(500).json({error: 'There was an error.'});
    }
});

router.delete('/:id', (req, res) => {
    const {id} = req.params;
    if (id) {
        _.each(opciones, (respuesta, i) => {
            if (respuesta.id == id) {
                opciones.splice(i, 1);
            }
        });
        res.json(opciones);
    }
});

module.exports = router;