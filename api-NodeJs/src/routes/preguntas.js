const { Router } = require('express');
const router = new Router();
const _ = require('underscore');

const preguntas = require('../preguntas.json');

router.get('/', (req, res) => {
    res.json(preguntas);
});

router.post('/', (req, res) => {
    const id = preguntas.length + 1;
    const { pregunta } = req.body;
    const newPregunta = { ...req.body, id };
    if (id && pregunta) {
        preguntas.push(newPregunta);
        res.json(preguntas);
    } else {
        res.status(500).json({error: 'There was an error.'});
    }
});

router.put('/:id', (req, res) => {
    const { id } = req.params;
    const { pregunta } = req.body;
    if (id && pregunta) {
        _.each(preguntas, (respuesta, i) => {
            if (respuesta.id === id) {
                respuesta.pregunta = pregunta;
            }
        });
        res.json(preguntas);
    } else {
        res.status(500).json({error: 'There was an error.'});
    }
});

router.delete('/:id', (req, res) => {
    const {id} = req.params;
    if (id) {
        _.each(preguntas, (respuesta, i) => {
            if (respuesta.id == id) {
                preguntas.splice(i, 1);
            }
        });
        res.json(preguntas);
    }
});

module.exports = router;